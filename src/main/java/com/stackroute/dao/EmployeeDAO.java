package com.stackroute.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.stackroute.model.Employee;

@Repository
public class EmployeeDAO {
	
	@Autowired
	private HibernateTemplate template;

	public HibernateTemplate getTemplate() {
		return template;
	}

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}
	
	public List<Employee> getAll(){
		
		List<Employee> employees = getTemplate().loadAll(Employee.class);
		return employees;
	}
	
	public Employee getById(int id) {
		
		Employee employee = (Employee)getTemplate().get(Employee.class, id);
		return employee;
	}
}
