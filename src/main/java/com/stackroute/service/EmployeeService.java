package com.stackroute.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stackroute.dao.EmployeeDAO;
import com.stackroute.model.Employee;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDAO empdao;
	
	@Transactional
	public List<Employee> getAll(){
		
		List<Employee> employees = empdao.getAll();
		return employees;
	}
	
	@Transactional
	public Employee getById(int id) {
		
		Employee employee = empdao.getById(id);
		return employee;
	}
}
