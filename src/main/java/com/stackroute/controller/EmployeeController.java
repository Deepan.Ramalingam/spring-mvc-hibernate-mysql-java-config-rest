package com.stackroute.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.model.Employee;
import com.stackroute.service.EmployeeService;

@RestController
@RequestMapping(value = "/rest")
public class EmployeeController {
	
	@Autowired
	private EmployeeService empService;
	
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public List<Employee> getEmployees(){
		
		return empService.getAll();
	}
	
	@RequestMapping(value = "employees/{id}", method = RequestMethod.GET)
	public Employee getEmployee(@PathVariable(name = "id") int id) {
		
		return empService.getById(id);
	}

}
